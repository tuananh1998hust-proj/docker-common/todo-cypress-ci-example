import React, { useState } from 'react';
import uuid from 'uuid';

const Input = props => {
  const { addTodo } = props;
  const [text, setText] = useState('');

  const handleClick = () => {
    const todo = {
      id: uuid(),
      text,
      iscompleted: false
    };
    addTodo(todo);
    setText('');
  };

  const handleChange = e => {
    setText(e.target.value);
  };

  return (
    <div className="add-todo">
      <input
        type="text"
        placeholder="add new works"
        value={text}
        onChange={e => handleChange(e)}
      />
      <button className="btn-add" onClick={handleClick}>
        ADD TODO
      </button>
    </div>
  );
};

export default Input;
