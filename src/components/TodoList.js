import React from 'react';

import TodoItem from './TodoItem';

const TodoList = props => {
  const { todolist, deleteTodo, completedTodo } = props;

  return (
    <div className="todo-list">
      {todolist.map(todo => (
        <TodoItem
          key={todo.id}
          item={todo}
          deleteTodo={deleteTodo}
          completedTodo={completedTodo}
        />
      ))}
    </div>
  );
};

export default TodoList;
